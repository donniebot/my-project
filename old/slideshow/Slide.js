import React from 'react';
import { Link } from "react-router";

class Slide extends React.Component {
 
  render() {
    let setClass = this.props.active ? 'show' : 'hide';
    return (
      <div className={setClass}>
        <img src={this.props.imagePath} alt={this.props.imageAlt} />
      </div>
    );
  }
}
export default Slide;