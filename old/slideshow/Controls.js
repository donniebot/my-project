import React, {Component} from 'react';
import { Link } from "react-router";

class Controls extends Component {

  render() {
    console.log('prev/next',this.props.prevPhoto)
    let prevPhoto = this.props.prevPhoto; 
    let nextPhoto = this.props.nextPhoto;
    return (
      <div className="controls">
        <div className="toggle toggle--prev" onClick={prevPhoto}><img src="images/prev-arrow.svg" alt="prev"/></div>
        <div className="toggle toggle--next" onClick={nextPhoto}><img src="images/next-arrow.svg" alt="next"/></div>
      </div>
    );
  }
}
export default Controls;