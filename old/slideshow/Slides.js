import React, {Component} from 'react';
import Slide from './Slide';
import { Link } from "react-router";

class Slides extends Component {
 
  render() {
    let currentSlide = this.props.data.currentSlide;
    let displayClass = 'hide'; 
    let slidesNodes = this.props.data.data.map(function (slideNode, index) {
      let isActive = currentSlide === index;
      if(currentSlide === index){
        displayClass = 'show';
      }
      return (
        <Slide active={isActive} key={slideNode.id} imagePath={slideNode.imagePath} imageAlt={slideNode.imageAlt} title={slideNode.title} subtitle={slideNode.subtitle} text={slideNode.text} action={slideNode.action} actionHref={slideNode.actionHref} />
      );
    });

    return (
      <div className="slides">
         {slidesNodes}
      </div>
    );
  }
}
export default Slides;