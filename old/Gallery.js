import React, {Component} from 'react';
import { Link } from "react-router";
import Slides from './slideshow/Slides';
import Controls from './slideshow/Controls';
import ImageData from '../data/images';

class Gallery extends Component {
  constructor(){
    super();
    this.state = {      
      currentSlide: 0,
      data: ImageData
    }
    this.prevPhoto = this.prevPhoto.bind(this);
    this.nextPhoto = this.nextPhoto.bind(this);
  }

  prevPhoto() {

   let showSlide = (this.state.currentSlide == 0)? 
      this.state.data.length -1 :
      this.state.currentSlide -1 ;

    this.setState({
      currentSlide: showSlide
    })
  }

  nextPhoto() {

   let showSlide = (this.state.currentSlide == this.state.data.length -1)? 
      this.state.currentSlide = 0 :
      this.state.currentSlide + 1 ;

    this.setState({
      currentSlide: showSlide
    });
  } 

  render() {
    return (
      <div className="home">
      <div className="slideshow">
        <Slides data={this.state} />
        <Controls prevPhoto={this.prevPhoto} nextPhoto={this.nextPhoto}/>
      </div>
      </div>
    )
  }
}
export default Gallery;
