import React, {Component} from 'react';
import Images from './Images';

class Preview extends Component {
 
  render() {

   let previewPhoto = this.props.previewPhoto;
   let previewImageElm = '';
   if(previewPhoto !== ''){
       previewImageElm = <Images source={previewPhoto}></Images>;
   }
console.log("Preview",previewPhoto)
    return (
        <div className="preview">{previewImageElm}</div>
    );
  }
}
export default Preview;