import React, {Component} from 'react';
import AutoResults from './AutoResults';

class Search extends Component {
 
  render() {
    let randomName = 'foo' + Math.random();
    let updateSearchWord = this.props.updateSearchWord;
    let sVal = this.props.searchWord;
    return (
      <div className="search">
        <form>
         <input className="search-word" name={randomName} type="text" value={sVal} onChange={updateSearchWord} />
        </form>
      </div>
    );
  }
}
export default Search;