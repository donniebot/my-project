import React, {Component} from 'react';
import Item from './Item';

class AutoResults extends Component {
  constructor(){
    super();
    this.onPreviewClick = this.onPreviewClick.bind(this);
  }

  onPreviewClick (e) {
    e.stopPropagation();
  }

  render() {
    let searchResults = this.props.searchResults; 
    let onRollOverResults = this.props.onRollOverResults;
    let onItemMouseLeave = this.props.onItemMouseLeave;
    let onPreviewClick = this.props.onPreviewClick;
    let previewItemClick = this.props.previewItemClick;

    let items = searchResults.map((result,i) =>
     (<Item className='result-items' key={i} result={result} onRollOverResults={onRollOverResults} onClick={onPreviewClick} previewItemClick={previewItemClick}></Item>)
    );

    return (
      <div className="auto-results">
       <ul>
         {items}
        </ul>
      </div>
    );
  }
}
export default AutoResults;