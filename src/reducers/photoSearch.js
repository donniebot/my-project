const initState = {
    photos: [
      {
        id         : "slide1",
        imagePath  : "../images/slide1.jpg",
        title      : "Charger(1968)",
        text       : "My First Car",
      },
      {
        id         : "slide2",
        imagePath  : "../images/slide2.jpg",
        title      : "Samurai(1987) before mods",
        text       : "Samurai before I made changes",
      },
      {
        id         : "slide3",
        imagePath  : "../images/slide3.jpg",
        title      : "Samurai(1987) after mods",
        text       : "Samurai after I graded it",
      }
    ],
    currentSearch: '',
    previewList: [],
    selectedPreviewPhoto: ''
  }
  
  const PHOTO_ADD = 'PHOTO_ADD'
  const CURRENT_SEARCH = 'CURRENT_SEARCH'
  const GET_PHOTOS = 'GET_PHOTOS'
  const CLEAR_CURRENT_SEARCH = 'CLEAR_CURRENT_SEARCH'
  const UPDATE_PREVIEW_LIST = 'UPDATE_PREVIEW_LIST'
  const CLEAR_PREVIEW_LIST = 'CLEAR_PREVIEW_LIST'
  const UPDATE_PREVIEW_PHOTO = 'UPDATE_PREVIEW_PHOTO'
  const CLEAR_PREVIEW_PHOTO = 'CLEAR_PREVIEW_PHOTO'
  
  export const updateCurrentSearchWord = (val) => ({type:CURRENT_SEARCH, payload: val})
  
  export const clearCurrentSearchWord = () => ({type:CLEAR_CURRENT_SEARCH})

  export const updatePreviewList = () => ({type:UPDATE_PREVIEW_LIST})

  export const clearPreviewList = () => ({type:CLEAR_PREVIEW_LIST})

  export const updatePreviewPhoto = (val) => ({type:UPDATE_PREVIEW_PHOTO, payload: val})

  export const clearPreviewPhoto = (val) => ({type:CLEAR_PREVIEW_PHOTO, payload: val})
  
  
  export default (state = initState, action) => {
    switch (action.type) {
      case UPDATE_PREVIEW_LIST:
        console.log("UPDATE_PREVIEW_LIST")
        const searchWord = state.currentSearch
        const imageData = state.photos
        let found = []
        if(searchWord){
          const regExp = new RegExp(`^${searchWord}{1}`)
          imageData.map(function(image){
            const imageTitle = image.title.toLowerCase()
            if(Boolean(imageTitle.match(regExp))){
              let imageObj = {}
              imageObj.title = image.title.toLowerCase()
              imageObj.id = image.id
              found.push(imageObj)
            }
          })
        }
        return {...state, previewList: found}
      case UPDATE_PREVIEW_PHOTO: 
        console.log("UPDATE_PREVIEW_PHOTO")
        return {...state, selectedPreviewPhoto: action.payload}
      case CLEAR_PREVIEW_PHOTO: 
        console.log("CLEAR_PREVIEW_PHOTO")
        return {...state, selectedPreviewPhoto: ''}        
      case CLEAR_PREVIEW_LIST:
        console.log("CLEAR_PREVIEW_LIST")
        return {...state, previewList: []}
      case PHOTO_ADD:
        console.log("PHOTO_ADD")
        return {...state, photos: state.photos.concat(action.payload)}
      case CURRENT_SEARCH:
        console.log("CURRENT_SEARCH")
        return {...state, currentSearch: action.payload}
      case CLEAR_CURRENT_SEARCH:
        console.log("CLEAR_CURRENT_SEARCH")
        return {...state, currentSearch: ''}
      case GET_PHOTOS:
        console.log("GET_PHOTOS")
        return {...state, photos: state.photos}
      default:
        console.log("DEFAULT")
        return state
    }
  }
  