import React, {Component} from 'react'
import { Link } from 'react-router'
import {connect} from 'react-redux'
import Search from '../components/molecules/Search'
import AutoResults from '../components/molecules/AutoResults'
import Preview from '../components/molecules/Preview'
import Flexbox from 'flexbox-react'
import { 
  currentSearch, 
  updateCurrentSearchWord, 
  clearCurrentSearchWord,
  photos,
  previewList, 
  updatePreviewList, 
  clearPreviewList, 
  selectedPreviewPhoto,
  updatePreviewPhoto,
  clearPreviewPhoto
} from '../reducers/photoSearch'

class Home extends Component {
  
  constructor(props){
    super();

    this.onRollOverResults = this.onRollOverResults.bind(this)
    this.bodyClick = this.bodyClick.bind(this)
    this.previewItemClick = this.previewItemClick.bind(this)
  }

  previewItemClick(e){

    const photoId = e.target.getAttribute('value')
    const photoTitle = e.target.getAttribute('title')
    console.log(photoTitle)
    this.props.history.pushState(null,"displayPhoto/"+photoId+"/"+photoTitle+"/")
  }

  bodyClick(){

    this.props.clearCurrentSearchWord()
    this.props.clearPreviewList()
    this.props.clearPreviewPhoto()
  }

  onRollOverResults(e) {

    let previewImageId = e.target.getAttribute('value')
    const photosData = this.props.photos
    let path = ''

    photosData.map(function(image){
      let imageId = image.id

      if(imageId === previewImageId){
        path = image.imagePath
      }
    })
    this.props.updatePreviewPhoto(path)
  }

  render() {

    let searchBox = 
      this.props.previewList.length ? 
        (<AutoResults 
          previewList={this.props.previewList} 
          onRollOverResults={this.onRollOverResults} 
          previewItemClick={this.previewItemClick}> 
          </AutoResults>)
          :
        ''
    return (
      <div className="home" onClick={this.bodyClick}>
        <Flexbox flexDirection="column" 
          justifyContent="center" 
          display="flex" 
          height="70%"> 

          <div className="search">
            <Preview 
              selectedPreviewPhoto={this.props.selectedPreviewPhoto}>
            </Preview>

            <Search 
              photos={this.props.photos} 
              currentSearch={this.props.currentSearch} 
              updateCurrentSearchWord={this.props.updateCurrentSearchWord} 
              photos={this.props.photos} 
              updateSearchWord={this.updateSearchWord} 
              updatePreviewList={this.props.updatePreviewList}>
            </Search>
            {searchBox}
          </div>

        </Flexbox>
      </div>
    )
  }
}

export default connect(
  (state) => state,
  { updateCurrentSearchWord, 
    clearCurrentSearchWord, 
    updatePreviewList,
    clearPreviewList,
    updatePreviewPhoto,
    clearPreviewPhoto
  }
)(Home)