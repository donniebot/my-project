import React from 'react';
import { Link } from "react-router";

class DisplayPhoto extends React.Component {
 
  render() {
    const { query } = this.props.location;
    const { params } = this.props;
    const photoTitle = this.props.params.photoTitle;
    let photoUrl = '../images/'+this.props.params.photoId+'.jpg';

    return (
      <div className="searchPage">
          <h1>{photoTitle}</h1>
          <img src={photoUrl}/>
      </div>
    );
  }
}
export default DisplayPhoto;