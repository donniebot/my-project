import React, {Component} from 'react'
import { Link } from 'react-router'
import { connect } from 'react-redux'

class Item extends Component {
 
  render() {
   let result = this.props.result;
    let onRollOverResults = this.props.onRollOverResults;
    let onItemMouseLeave = this.props.onItemMouseLeave;
    let previewItemClick = this.props.previewItemClick;

    return (
        <li onMouseEnter={onRollOverResults} onMouseLeave={onItemMouseLeave} onClick={previewItemClick} value={result.id} title={result.title}>
          {result.title}
         
        </li>
    );
      
    return (
        <pre><code>{JSON.stringify(props,null,2)}</code></pre>  
    )
  }
}

export default Item


/*
import React, {Component} from 'react'
import styled from 'styled-components'
import { Link } from 'react-router'
import { connect } from 'react-redux'

const StyledLi = styled.li`
  font-style: normal;
  font-size: 16px;
  list-style: none;
  font-family: arial;
  text-align: left;
  clear: both;

  &:hover{
    background-color: #F0F0F0;
  }
`


const Item = (props) => {

  return (
    <li onMouseEnter={props.onRollOverResults} onMouseLeave={props.onItemMouseLeave} onClick={props.previewItemClick} value={props.result.id}>
      {props.result.title}
      <pre><code>{JSON.stringify(props,null,2)}</code></pre>
    </li>
  );
}

function mapState(state) {
  return state
}
export default connect(mapState)(Item)

*/