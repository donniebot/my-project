import React, {Component} from 'react'
import { Link } from 'react-router'
import ImageData from '../../../data/images'
import Search from '../../molecules/Search'
import AutoResults from '../../molecules/AutoResults'
import Preview from '../../molecules/Preview'
import Flexbox from 'flexbox-react'

class Home extends Component {
  constructor(props){
    super();
    this.state = {      
      currentSlide: 0,
      data: ImageData,
      searchWord: 'dd',
      previewPhoto: '',
      searchResults: '',
    }

    this.updateSearchWord = this.updateSearchWord.bind(this)
    this.onRollOverResults = this.onRollOverResults.bind(this)
    this.onItemMouseLeave = this.onItemMouseLeave.bind(this)
    this.bodyClick = this.bodyClick.bind(this)
    this.previewItemClick = this.previewItemClick.bind(this)

  }

  previewItemClick(e){
    let photoId = e.target.getAttribute('value')
    this.props.history.pushState(null,"displayPhoto/"+photoId)
  }

  bodyClick(){

    this.setState({
      searchResults: '',
      previewPhoto: ''
    })

  }

  updateSearchWord(e){
    let searchWord = (e.target.value).toLowerCase()
    let imageData = this.state.data
    let found = []
    if(searchWord){
      let regExp = new RegExp(`^${searchWord}{1}`)
    
      imageData.map(function(image){

        let imageTitle = image.title.toLowerCase()

        if(Boolean(imageTitle.match(regExp))){
          found.push(image)
        }

      });
    }
    this.setState({
      searchResults:found
    });
  }

  onRollOverResults(e) {

    let previewImageId = e.target.getAttribute('value')
    let imageData = this.state.data
    let path = ''


    imageData.map(function(image){

      let imageId = image.id

      if(imageId === previewImageId){
        path = image.imagePath
      }

    });
    this.setState({
        previewPhoto: path
    })

  }

  onItemMouseLeave() {
    this.setState({
      previewPhoto: ''
    })
  }

  render() {

    let searchBox = 
      this.state.searchResults.length ? 
        (<AutoResults searchResults={this.state.searchResults} onRollOverResults={this.onRollOverResults} onItemMouseLeave={this.onItemMouseLeave} previewItemClick={this.previewItemClick}> </AutoResults>):
        ''

    return (
      <div className="home" onClick={this.bodyClick}>
        <Flexbox flexDirection="column" justifyContent="center" display="flex" height="70%"> 
          <div className="search">
            <Preview previewPhoto={this.state.previewPhoto}></Preview>
            <Search updateSearchWord={this.updateSearchWord}></Search>
            {searchBox}
          </div>
        </Flexbox>
      </div>
    )
  }
}
export default Home;
