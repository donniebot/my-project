import React, {Component} from 'react'
import Image from '../../atoms/Image'
import styled from 'styled-components'

const StyledDiv = styled.div`
    width: 100%;
    position: absolute;
    bottom: 15px;
`

const StyledImage = styled(Image)`
    width: 100px;
    height: 100px;
    padding-bottom: 20px;
    bottom: 22px;
    left: 0;
`

const Preview = ({
        className,
        selectedPreviewPhoto
    }) => {

    let previewImageElm = false

    if(selectedPreviewPhoto){
      previewImageElm = <StyledImage source={selectedPreviewPhoto}></StyledImage>
    }
    
    return (
        <StyledDiv className={className}>
            {previewImageElm && <StyledImage source={selectedPreviewPhoto}></StyledImage>}
        </StyledDiv>
    )
}

export default Preview;