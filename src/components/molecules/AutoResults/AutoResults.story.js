import React from 'react'
import { storiesOf } from '@storybook/react'
import AutoResults from './AutoResults';

const itemClick = () => {
  console.log('clicked')
}

const mouseLeave = () => {
  console.log('mouse leave')
}

const rollOver = () => {
  console.log('rollover')
}

const props = {
  searchResults: [{
    action: 'Slide 3 Image Action',
    actionHref: 'href',
    id: 'slide3',
    imageAlt: 'Samurai before mods',
    imagePath: '../images/slide2.jpg',
    subtitle: 'Slide 3 Image SubTitle',
    text: 'Slide 3 Image Text',
    title: 'Samurai after mods'
  },
  {
    action: 'Slide 4 Image Action',
    actionHref: 'href',
    id: 'slide3',
    imageAlt: 'Samurai after mods',
    imagePath: '../images/slide3.jpg',
    subtitle: 'Slide 3 Image SubTitle',
    text: 'Slide 3 Image Text',
    title: 'Samurai after mods'
  }
],
  onRollOverResults: rollOver,
  onItemMouseLeave: mouseLeave,
  previewItemClick: itemClick
}


const story = storiesOf('molecules/AutoResults', module)
  .add('result', () => {
    return (
      <AutoResults
        {...props}
      />
    )
  })

export default story
