import React from 'react'
import { Router, Route, IndexRoute, Link, hashHistory } from 'react-router'

import Home from './pages/Home'
import Layout from './pages/Layout'
import DisplayPhoto from './pages/search/DisplayPhoto'

class App extends React.Component {

    render(){
        let myState = this.props;
        return (
            <div>
                <Router history={ hashHistory }>
                    <Route path="/" component={Layout}>
                        <IndexRoute component={Home}></IndexRoute>
                        <Route path="home" component={Home}></Route>
                        <Route path="displayPhoto/:photoId/:photoTitle" component={DisplayPhoto}></Route>
                    </Route> 
                </Router>
            </div>
        );
    }
}
export default App;