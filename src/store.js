import {createStore} from 'redux'
import reducer from './reducers/photoSearch'

export default createStore(reducer)
